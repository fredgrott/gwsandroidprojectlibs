<?cs def:custom_masthead() ?>
<div id="header">
      <div id="headerLeft">
           <a href="<?cs var:toroot ?>index.html" tabindex="-1"><img
              src="<?cs var:toroot ?>assets/images/project_logo.png" alt="<?cs var:project.name ?>" /></a>
          <?cs include:"header_tabs.cs" ?>     <?cs # The links are extracted so we can better manage localization ?>
      </div>
       <div id="headerRight">
      <?cs call:default_search_box() ?>
      <?cs if:reference && reference.apilevels ?>
        <?cs call:default_api_filter() ?>
      <?cs /if ?>
    </div>
</div><!-- header -->
<?cs /def ?>